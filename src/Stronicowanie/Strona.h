/*
 * Strona.h
 *
 *  Created on: 14 paź 2013
 *      Author: rafal
 */

#ifndef STRONA_H_
#define STRONA_H_

#include <iostream>
#include <string>
#include <OgreRoot.h>
#include <OgreEntity.h>
#include <OgreMeshManager.h>

class Strona {
public:
	enum QueryFlags
	{
        MOVING_OBJ = 1<<0,
        GROUND_OBJ = 1<<1,
        STATIC_OBJ = 1<<2
	};
    enum MeshBuildType
    {
        MBT_PLANE,
        MBT_CURVED_ILLUSION_PLANE,
        MBT_CURVED_PLANE
    };
	Strona(Ogre::SceneNode* bi);
	virtual ~Strona();
	int  add(int x,int y);
	static int global;
	Ogre::Plane mPlane;
	Ogre::SceneNode* bigPlain;
    Ogre::SceneNode* cn0; //lokalna scena - ten placek
    Ogre::Entity* p0; //plane entyti

    int xLocalPos;
    int yLocalPos;
private:
    struct MeshBuildParams
    {
    	MeshBuildType type;
        Ogre::Plane plane;
        Ogre::Real width;
        Ogre::Real height;
        Ogre::Real curvature;
        int xsegments;
        int ysegments;
        bool normals;
        unsigned short numTexCoordSets;
        Ogre::Real xTile;
        Ogre::Real yTile;
        Ogre::Vector3 upVector;
        Ogre::Quaternion orientation;
        Ogre::HardwareBuffer::Usage vertexBufferUsage;
        Ogre::HardwareBuffer::Usage indexBufferUsage;
        bool vertexShadowBuffer;
        bool indexShadowBuffer;
        int ySegmentsToKeep;
    };

	void tesselate2DMesh(Ogre::SubMesh* sm, unsigned short meshWidth, unsigned short meshHeight,
			bool doubleSided, Ogre::HardwareBuffer::Usage indexBufferUsage, bool indexShadowBuffer);
	void loadManualPlane(Ogre::Mesh* pMesh,MeshBuildParams& params);
    Ogre::Image cropImage(const Ogre::Image source, size_t offsetX, size_t offsetY, size_t width, size_t height);
    Ogre::MaterialPtr mat; ///material plajnu
};

#endif /* STRONA_H_ */
