/*
 * Stronicowanie.cpp
 *
 *  Created on: 14 paź 2013
 *      Author: rafal
 */

#include "Stronicowanie.h"
#include <iostream>
#include "../BaseApplication.h"

Ogre::Image Stronicowanie::map; // definujemy statyczna zmienna
char* Stronicowanie::pSigned;

Stronicowanie::Stronicowanie(Ogre::SceneNode* bi) : renderSpace(3) {
	// TODO Auto-generated constructor stub=
	bigPlain =bi;
}
//laduje mape
void Stronicowanie::loadMap(Ogre::String bmpName) {
	map.load(bmpName,"General");
    pSigned=(char*)Stronicowanie::map.getData();
}

bool Stronicowanie::findPlaneOnList(int x,int y) {
	Strona* bufor;
	for (list<Strona*>::iterator it = lista.begin(); it != lista.end(); it++) {
		bufor=it.operator *();
		if(bufor->xLocalPos==x && bufor->yLocalPos==y)
				return true;
	}
	return false;
}

Ogre::Real Stronicowanie::getDistanceToPlayerPos(int x, int z) {
	Ogre::Vector2 pos1 = Ogre::Vector2(x,z);
	Ogre::Vector2 pos2 = Ogre::Vector2(last_x,last_y);
	return (pos2-pos1).length();
}

void Stronicowanie::removeOldPlanes() {
	Strona* bufor;
	Ogre::Real dist=0;
	for (list<Strona*>::iterator it = lista.begin(); it != lista.end(); it++) {
			bufor=it.operator *();
			dist=this->getDistanceToPlayerPos(bufor->xLocalPos,bufor->yLocalPos);
			if(abs(dist)>=1.41421*renderSpace) {
				bufor->~Strona();
				it =lista.erase(it);
				}
		}
	if(lista.size()>pow((float)(pow((float)renderSpace,2)+1),2)-4)
		this->removeOldPlanes();
}

bool Stronicowanie::dodajStrone(int x, int y) {

	int buf_x=round((float)x/500);
	int buf_y=round((float)y/500);
	int stat = 0;
	try {
		if(last_x!=buf_x || last_y!=buf_y) { // jesli nadepnieto nowy placek
			last_y=buf_y;
			last_x=buf_x;

			for(int ix=buf_x-renderSpace;ix<=buf_x+renderSpace;ix++) {
				for(int iy=buf_y-renderSpace;iy<=buf_y+renderSpace;iy++){
					if(this->findPlaneOnList(ix,iy)==false) {
						Strona* nw=new Strona(bigPlain);
						stat=nw->add(ix,iy);
						if(stat>=0)
							lista.push_back(nw);
						}
				}
			}
			this->removeOldPlanes();
		}
	} catch (Ogre::Exception& e) {
		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL,e.getFullDescription());
	}
return true;
}

Ogre::Plane Stronicowanie::getFirstPlane() {
  list<Strona*>::iterator iter = lista.begin();
  return iter.operator *()->mPlane;
}
void drawCirclePixel(int x, int y,char step,char max) {

}
/*
 * Generates ID for material, Scene and Entiti
 */
Ogre::String getNamePref(int x,int y) {
    int locH = Stronicowanie::map.getHeight() / 2;
    int locW = Stronicowanie::map.getWidth() / 2;
    int ramie =25;

    int by=(y*ramie)+locH;
    int bx=(x*ramie)+locW;

	ostringstream byStr;
	ostringstream bxStr;

	bxStr << by;
	byStr << bx;

	Ogre::String name = "x:"+bxStr.str()+"y:"+byStr.str();
	return name;
}
void Stronicowanie::GetMeshInformation(const Ogre::Mesh* const mesh,
        size_t &vertex_count,
        Ogre::Vector3* &vertices,
        size_t &index_count,
        unsigned int* &indices,
        const Ogre::Vector3 &position,
        const Ogre::Quaternion &orient,
        const Ogre::Vector3 &scale)
{
bool added_shared = false;
size_t current_offset = 0;
size_t shared_offset = 0;
size_t next_offset = 0;
size_t index_offset = 0;

vertex_count = index_count = 0;

// Calculate how many vertices and indices we're going to need
for ( unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i){
	Ogre::SubMesh* submesh = mesh->getSubMesh(i);
	// We only need to add the shared vertices once
	if(submesh->useSharedVertices){
			if( !added_shared ){
				vertex_count += mesh->sharedVertexData->vertexCount;
				added_shared = true;
				}
		}else{
		  vertex_count += submesh->vertexData->vertexCount;
		}
	// Add the indices
	index_count += submesh->indexData->indexCount;
	}

// Allocate space for the vertices and indices
vertices = new Ogre::Vector3[vertex_count];
indices  = new unsigned int[index_count];

added_shared = false;

// Run through the submeshes again, adding the data into the arrays
for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i){
Ogre::SubMesh* submesh = mesh->getSubMesh(i);
Ogre::VertexData* vertex_data = submesh->useSharedVertices ? mesh->sharedVertexData : submesh->vertexData;

if ((!submesh->useSharedVertices) || (submesh->useSharedVertices && !added_shared)){
	if(submesh->useSharedVertices){
		added_shared = true;
		shared_offset = current_offset;
	}

const Ogre::VertexElement* posElem =
vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);

Ogre::HardwareVertexBufferSharedPtr vbuf =
vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());

unsigned char* vertex =
static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));

// There is _no_ baseVertexPointerToElement() which takes an Ogre::Real or a double
//  as second argument. So make it float, to avoid trouble when Ogre::Real will
//  be comiled/typedefed as double:
//Ogre::Real* pReal;
float* pReal;

for( size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize()){
	posElem->baseVertexPointerToElement(vertex, &pReal);
	Ogre::Vector3 pt(pReal[0], pReal[1], pReal[2]);
	vertices[current_offset + j] = (orient * (pt * scale)) + position;
}

vbuf->unlock();
next_offset += vertex_data->vertexCount;
}

Ogre::IndexData* index_data = submesh->indexData;
size_t numTris = index_data->indexCount / 3;
Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer;

bool use32bitindexes = (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT);

unsigned int* pLong = static_cast<unsigned int*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
unsigned short* pShort = reinterpret_cast<unsigned short*>(pLong);

size_t offset = (submesh->useSharedVertices)? shared_offset : current_offset;

if ( use32bitindexes ){
for ( size_t k = 0; k < numTris*3; ++k){
	indices[index_offset++] = pLong[k] + static_cast<unsigned long>(offset);
	}
}else{
 for ( size_t k = 0; k < numTris*3; ++k){
	indices[index_offset++] = static_cast<unsigned long>(pShort[k]) +
							  static_cast<unsigned long>(offset);
	}
}

ibuf->unlock();
current_offset = next_offset;
}
}

void Stronicowanie::drawMouse(float x,float y) {

	float buf_x=(x+250)/500;
	float buf_y=(y+250)/500;
	float xplane;
	float yplane;
	float xoffset=std::modf(buf_x,&xplane);
	float yoffset=std::modf(buf_y,&yplane);
	xoffset+=1;
	yoffset+=1;
	float bin;
	xoffset=std::modf(xoffset,&bin);
	yoffset=std::modf(yoffset,&bin);
	int bx=round((float)x/500);
	int by=round((float)y/500);
	try {
	Ogre::Entity* buf = BaseApplication::mSceneMgr->getEntity("GroundEntity"+getNamePref(bx,by));
	Ogre::HardwareVertexBufferSharedPtr vbuf
		= buf->getVertexDataForBinding()
		->vertexBufferBinding->getBuffer(0);

	size_t i = 0;
	size_t sizeV=vbuf->getVertexSize(); // vertex size in byles

	Ogre::ABGR color; // kolor
	for(int y1=0;y1<26;y1++){
	    	for(int x1=26;x1>0;x1--){
	    		if(i < vbuf->getNumVertices()) {
	    			vbuf->readData((i*sizeV+3*sizeof(float)),sizeof(Ogre::ABGR), &color);
	    			color =Ogre::ColourValue(0.2,0.2,0.63,1).getAsABGR();
	    			vbuf->writeData((i*sizeV+3*sizeof(float)),sizeof(Ogre::ABGR), &color);
	    			}
	    		i++;
	    	}
	 }
} catch (Ogre::Exception& e) {
		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL,"TEXTURES - Strona.cpp");
		Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL,e.getFullDescription());
	}
}
// raycast from a point in to the scene.
// returns success or failure.
// on success the point is returned in the result.
bool Stronicowanie::RaycastFromPoint(const Ogre::Vector3 &point,const Ogre::Vector3 &normal,Ogre::Vector3 &result)
{
	Ogre::RaySceneQuery*  m_pray_scene_query= BaseApplication::mSceneMgr->createRayQuery(Ogre::Ray());
	    if (NULL == m_pray_scene_query){
	      return (false);
	    }
	    m_pray_scene_query->setSortByDistance(true);
    // create the ray to test
    Ogre::Ray ray(Ogre::Vector3(point.x, point.y, point.z), Ogre::Vector3(normal.x, normal.y, normal.z));

    // check we are initialised
    if (m_pray_scene_query != NULL) {
        // create a query object
        m_pray_scene_query->setRay(ray);
        // execute the query, returns a vector of hits
        if (m_pray_scene_query->execute().size() <= 0) {
            // raycast did not hit an objects bounding box
            return (false);
        }
    } else {
        return (false);
    }

    // at this point we have raycast to a series of different objects bounding boxes.
    // we need to test these different objects to see which is the first polygon hit.
    // there are some minor optimizations (distance based) that mean we wont have to
    // check all of the objects most of the time, but the worst case scenario is that
    // we need to test every triangle of every object.
    Ogre::Real closest_distance = -1.0f;
    Ogre::Vector3 closest_result;
    Ogre::RaySceneQueryResult &query_result = m_pray_scene_query->getLastResults();
    for (size_t qr_idx = 0; qr_idx < query_result.size(); qr_idx++)
    {
        // stop checking if we have found a raycast hit that is closer
        // than all remaining entities
        if ((closest_distance >= 0.0f) &&
            (closest_distance < query_result[qr_idx].distance))
        {
             break;
        }

        // only check this result if its a hit against an entity
        if ((query_result[qr_idx].movable != NULL) &&
            (query_result[qr_idx].movable->getMovableType().compare("Entity") == 0))
        {
            // get the entity to check
            Ogre::Entity *pentity = static_cast<Ogre::Entity*>(query_result[qr_idx].movable);

            // mesh data to retrieve
            size_t vertex_count;
            size_t index_count;
            Ogre::Vector3 *vertices;
            unsigned int *indices;

            // get the mesh information
            GetMeshInformation(pentity->getMesh().get(), vertex_count, vertices, index_count, indices,
        		 pentity->getParentNode()->_getDerivedPosition(),
        		 pentity->getParentNode()->_getDerivedOrientation(),
        		 pentity->getParentNode()->_getDerivedScale());

            // test for hitting individual triangles on the mesh
            bool new_closest_found = false;
            for (int i = 0; i < static_cast<int>(index_count); i += 3)
            {
                // check for a hit against this triangle
                std::pair<bool, Ogre::Real> hit = Ogre::Math::intersects(ray, vertices[indices[i]],
                    vertices[indices[i+1]], vertices[indices[i+2]], true, false);

                // if it was a hit check if its the closest
                if (hit.first)
                {
                    if ((closest_distance < 0.0f) ||
                        (hit.second < closest_distance))
                    {
                        // this is the closest so far, save it off
                        closest_distance = hit.second;
                        new_closest_found = true;
                    }
                }
            }

         // free the verticies and indicies memory
            delete[] vertices;
            delete[] indices;

            // if we found a new closest raycast for this object, update the
            // closest_result before moving on to the next object.
            if (new_closest_found)
            {
                closest_result = ray.getPoint(closest_distance);
            }
        }
    }

    // return the result
    if (closest_distance >= 0.0f){
        // raycast success
        result = closest_result;
        return (true);
    } else {
        // raycast failed
        return (false);
    }
}
Stronicowanie::~Stronicowanie() {
}

