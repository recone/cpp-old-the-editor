/*
 * Teren.cpp
 *
 *  Created on: 2013-03-07
 *  Author: Rafal Lesniewski
 */


#include <iostream>
#include <string>
#include "OgrePrerequisites.h"
#include "Player.h"
#include "../BaseApplication.h"
using namespace std;

Ogre::SceneManager* BaseApplication::mSceneMgr;

Player::Player(Ogre::SceneNode* ent) {
	// TODO Auto-generated constructor stub
	sizeVertex=0;
	krok = Ogre::Vector3(0,0,0);
	krok2= Ogre::Vector3(0,0,0);
	cel  = Ogre::Vector3(0,0,0);
	obj=ent;
}


void Player::preparePlayerNode() {
	ninia  = obj->createChildSceneNode("ninia1");
		ninEnt = BaseApplication::mSceneMgr->createEntity("robot2","ninja.mesh");
		ninEnt->setCastShadows(true);
		ninEnt->setQueryFlags(STATIC_OBJ);
	ninia->attachObject(ninEnt);
	ninia->scale(0.6,0.6,0.6);
	ninia->setPosition(Ogre::Vector3(0,0,0));
}

Ogre::Entity* Player::getPlayerEnt() {
	return ninEnt;
}

Ogre::SceneNode* Player::getPlayerNode(){
	return ninia;
}

void Player::movePlayer(Ogre::Real krokR) {
Ogre::Ray xc(krok2,kierunek);
if(kierunek==Ogre::Vector3(0,0,0)) return;

speed=300*krokR;
if( move==true) {
	if(cel!=xc.getPoint(point) && point>=0) {
		krok=xc.getPoint(point);
	  if((cel - krok).length()<100) {
		  speed*=((cel - krok).length()*0.002)+0.5;
		  }
	  ninEnt->getAnimationState("Walk")->addTime(speed*0.0125);
      if((cel - krok).length()>1) {
    	  point+=speed;
    	  ninia->setPosition(krok);
        } else {
          move=false;
        }
    } else {
    	ninEnt->getAnimationState("Walk")->setEnabled(false);
    	move=false;
    }
    Ogre::Vector3 xx = loCam->getPosition();
    xx.x=krok.x+500;
    xx.z=krok.z+500;
    loCam->setPosition(xx);

  }
}
/*
 * Mova camera after player
 */

void Player::controlCamera(Ogre::Camera* cam) {
	loCam=cam;
}

Player::~Player() {
	// TODO Auto-generated destructor stub
}
