/*
 * Teren.h
 *
 *  Created on: 2013-03-07
 *      Author: rafal
 */

#ifndef TEREN_H_
#define TEREN_H_

#include <OgreRoot.h>
#include <OgreEntity.h>
#include "OgreString.h"

#include "OgreMaterialManager.h"

class Player {
	private:
		Ogre::Camera* loCam; // wskaznik do lokalnej kamery
		Ogre::SceneNode* ninia; // scena playera
		Ogre::Entity* ninEnt; // entity playera
	public:
		enum QueryFlags
		{
		        MOVING_OBJ = 1<<0,
		        GROUND_OBJ = 1<<1,
		        STATIC_OBJ = 1<<2
		};
		Player(Ogre::SceneNode* ent);
		void preparePlayerNode();
		void movePlayer(Ogre::Real krokR);
		void controlCamera(Ogre::Camera* cam);

		virtual Ogre::Entity* getPlayerEnt();
		virtual Ogre::SceneNode* getPlayerNode();

		virtual ~Player();

		bool move;
		Ogre::Image map;   //obrazek w pamieci
		Ogre::SceneNode* obj; // plain pointer bigPlain
		char* pSigned;	   // mapabitowa w vektorze
		Ogre::HardwareVertexBufferSharedPtr vbuf; // wskaznik do sprzetowego bufora terenu

		Ogre::Vector3 kierunek; // vektor kierunku
		Ogre::Vector3 cel; 		// cel drogi
		Ogre::Vector3 krok; 	// lokalny spacer na mash
		Ogre::Vector3 krok2; 	// globalny spacer po bitmapie

		size_t sizeVertex; // liczba vertexow na mash

		Ogre::Real point;
		Ogre::Real speed;
	};

#endif /* TEREN_H_ */
