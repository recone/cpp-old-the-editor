/*
 * Stronicowanie.h
 *
 *  Created on: 14 paź 2013
 *      Author: rafal
 */

#ifndef STRONICOWANIE_H_
#define STRONICOWANIE_H_

#include <list>
#include <OgreRoot.h>
#include <OgreEntity.h>
#include <OgreMeshManager.h>

#include "Strona.h"

using namespace std;
class Stronicowanie {
public:
	Stronicowanie(Ogre::SceneNode* bi);
	~Stronicowanie();
	bool dodajStrone(int x, int y); // tworzy 9 nowych plackow jesli pozycja playera sie zmienila
	Ogre::Plane getFirstPlane();
	bool findPlaneOnList(int x,int y); // plane in pos x y exists?
	Ogre::Real getDistanceToPlayerPos(int x, int z); // liczy odleglosc placka od playera
	void removeOldPlanes();
	void drawMouse(float x,float y);
	void loadMap(Ogre::String bmpName);
	Ogre::SceneNode* bigPlain;
	list<Strona*> lista;

	Ogre::Real last_x;
	Ogre::Real last_y;
	int renderSpace; //ile plackow rysowac?

	static Ogre::Image map;	  //obrazek w pamieci
	static char* pSigned;
	void GetMeshInformation(const Ogre::Mesh* const mesh,size_t &vertex_count,Ogre::Vector3* &vertices,size_t &index_count,
	unsigned int* &indices,const Ogre::Vector3 &position,const Ogre::Quaternion &orient,const Ogre::Vector3 &scale);
	bool RaycastFromPoint(const Ogre::Vector3 &point,const Ogre::Vector3 &normal,Ogre::Vector3 &result);
private:

   void drawCirclePixel(int x, int y,char step,char max);
};

#endif /* STRONICOWANIE_H_ */
