#include <iostream>
#include <string>
#include "BaseApplication.h"


using namespace std;
Ogre::Root* BaseApplication::mRoot;
//-------------------------------------------------------------------------------------
BaseApplication::BaseApplication(void)
    : mCamera(0),mWindow(0), mResourcesCfg(Ogre::StringUtil::BLANK),  mPluginsCfg(Ogre::StringUtil::BLANK),
    mTrayMgr(0),  mCameraMan(0),   mCursorWasVisible(false),
    mShutDown(false), mInputManager(0),   mMouse(0),  mKeyboard(0){
}

//-------------------------------------------------------------------------------------
BaseApplication::~BaseApplication(void){
    if (mTrayMgr) delete mTrayMgr;
    if (mCameraMan) delete mCameraMan;
    Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);
    windowClosed(mWindow);
    delete mRoot;
}

//-------------------------------------------------------------------------------------
bool BaseApplication::configure(void){
    if(mRoot->showConfigDialog()){
        mWindow = mRoot->initialise(true);
        return true;
    } else {
        return false;
    }
}

//-------------------------------------------------------------------------------------
void BaseApplication::chooseSceneManager(void) {
	Ogre::LogManager::getSingletonPtr()->logMessage("*** CHOICE SCENEMANAGER ***");
    mSceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC);
   //mSceneMgr->setShadowTextureSize(256);
   //mSceneMgr->setShadowCasterRenderBackFaces(true);
   //mSceneMgr->setShadowTextureCount(64);
   //mSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_ADDITIVE);
   mSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
   mSceneMgr->setAmbientLight(Ogre::ColourValue(0.01,0.01,0.01));
/*
 * FOG
 */
   Ogre::ColourValue fadeColour(0,0,0);
   mSceneMgr->setFog(Ogre::FOG_EXP, fadeColour, 0.00018);
/*
 * Ceate big Plain
 */
   bigPlain=mSceneMgr->getRootSceneNode()->createChildSceneNode();
	pointLight = mSceneMgr->createLight("pointLight");
	pointLight->setType(Ogre::Light::LT_POINT);
	pointLight->setPosition(Ogre::Vector3(0, 2500, 0));
	pointLight->setDiffuseColour(0.1, 0.1, 0.1);
	pointLight->setSpecularColour(10, 10, 10);
	pointLight->setCastShadows(true);
/*
 * Create player and assign to bigPlain
 */
    player = new Player(bigPlain);
    player->preparePlayerNode();
/*
 *  Create pointer to player ent and player node
 */
    niniaNode1 = player ->getPlayerNode();
    niniaEnt1  = player->getPlayerEnt();
/*
 * Create terain
 */
    st = new Stronicowanie(bigPlain);
    st->loadMap("mapa2.png"); /// laduje mape planszy
    st->dodajStrone(0,0);

}
//-------------------------------------------------------------------------------------
void BaseApplication::createCamera(void)
{
	Ogre::LogManager::getSingletonPtr()->logMessage("*** CREATE CMERA ***");
    // Create the camera
    mCamera = mSceneMgr->createCamera("FirstCamera");
    //Position it at 500 in Z direction
    mCamera->setPosition(Ogre::Vector3(500,500,500));
    mCamera->lookAt(Ogre::Vector3(0,0,0));
    mCamera->setNearClipDistance(6);
    player->controlCamera(mCamera);
    mCameraMan = new OgreBites::SdkCameraMan(mCamera);
}
//-------------------------------------------------------------------------------------
void BaseApplication::createFrameListener(void)
{
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");
    OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    mWindow->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

    mInputManager = OIS::InputManager::createInputSystem( pl );
    mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject( OIS::OISKeyboard, true ));
    mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject( OIS::OISMouse, true ));

    mMouse->setEventCallback(this);
    mKeyboard->setEventCallback(this);

    //Set initial mouse clipping size
    windowResized(mWindow);

    //Register as a Window listener
    Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);

    mTrayMgr = new OgreBites::SdkTrayManager("InterfaceName", mWindow, mMouse, this);
    mTrayMgr->showFrameStats(OgreBites::TL_BOTTOMLEFT);
    //mTrayMgr->showLogo(OgreBites::TL_BOTTOMRIGHT);
    //mTrayMgr->showCursor("Examples/Cursor");

    mRoot->addFrameListener(this);
}

//-------------------------------------------------------------------------------------
void BaseApplication::destroyScene(void)
{
}
//-------------------------------------------------------------------------------------
void BaseApplication::createViewports(void)
{
    // Create one viewport, entire window
	Ogre::LogManager::getSingletonPtr()->logMessage("*** CREATE VIEWPORT ***");
    Ogre::Viewport* vp = mWindow->addViewport(mCamera);

    Ogre::CompositorManager::getSingleton().addCompositor(mWindow->getViewport(0), "blur");
    Ogre::CompositorManager::getSingleton().setCompositorEnabled(mWindow->getViewport(0), "blur", true);
    vp->setBackgroundColour(Ogre::ColourValue(0,0,0));

    // Alter the camera aspect ratio to match the viewport
    mCamera->setAspectRatio(
    Ogre::Real(vp->getActualWidth()) / Ogre::Real(vp->getActualHeight()));
}
//-------------------------------------------------------------------------------------
void BaseApplication::setupResources(void)
{
    // Load resource paths from config file
    Ogre::ConfigFile cf;
    cf.load(mResourcesCfg);

    // Go through all sections & settings in the file
    Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

    Ogre::String secName, typeName, archName;
    while(seci.hasMoreElements()){
        secName = seci.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); ++i){
            typeName = i->first;
            archName = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(
            archName, typeName, secName);
        }
    }
}
//-------------------------------------------------------------------------------------
void BaseApplication::createResourceListener(void)
{

}
//-------------------------------------------------------------------------------------
void BaseApplication::loadResources(void)
{
	//Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups(true);
	//OgreRoot::getSingleton().initialise(true);
	Ogre::LogManager::getSingletonPtr()->logMessage("*** LOAD RESORCES ***");
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
	Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);

}
//-------------------------------------------------------------------------------------
void BaseApplication::go(void)
{
#ifdef _DEBUG
    mResourcesCfg = "resources.cfg";
    mPluginsCfg = "plugins.cfg";
#else
    mResourcesCfg = "resources.cfg";
    mPluginsCfg = "plugins.cfg";
#endif

    if (!setup())
        return;

    mRoot->startRendering();

    // clean up
    destroyScene();
}
//-------------------------------------------------------------------------------------
bool BaseApplication::setup(void)
{
    mRoot = new Ogre::Root(mPluginsCfg);
    setupResources();

    bool carryOn = configure();
    if (!carryOn) return false;

    loadResources();
    chooseSceneManager();
    createCamera();
    createViewports();
    createFrameListener();


    return true;
};
//-------------------------------------------------------------------------------------
bool BaseApplication::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
    if(mWindow->isClosed()) return false;
    if(mShutDown) return false;

    //Need to capture/update each device
    mKeyboard->capture();
    mMouse->capture();
    //mAnimationState->addTime(evt.timeSinceLastFrame*2);

    player->movePlayer(evt.timeSinceLastFrame);
    st->dodajStrone(player->krok.x,player->krok.z);

    mTrayMgr->frameRenderingQueued(evt);
    if (!mTrayMgr->isDialogVisible()) {
        mCameraMan->frameRenderingQueued(evt);
    }

    return true;
}
//-------------------------------------------------------------------------------------
bool BaseApplication::keyPressed( const OIS::KeyEvent &arg )
{
    if (mTrayMgr->isDialogVisible())
    	return true;
    if (arg.key == OIS::KC_F){
        mTrayMgr->toggleAdvancedFrameStats();
    // toggle visibility of even rarer debugging details
    } else if (arg.key == OIS::KC_G){
    } else if (arg.key == OIS::KC_Z) {

    }
    else if (arg.key == OIS::KC_C) {

    }
    else if (arg.key == OIS::KC_X) {
    }
    else if (arg.key == OIS::KC_T) {

    }
    else if (arg.key == OIS::KC_R)   // cycle polygon rendering mode
    {
        Ogre::String newVal;
        Ogre::PolygonMode pm;

        switch (mCamera->getPolygonMode())
        {
        case Ogre::PM_SOLID:
            newVal = "Wireframe";
            pm = Ogre::PM_WIREFRAME;
            break;
        case Ogre::PM_WIREFRAME:
            newVal = "Points";
            pm = Ogre::PM_POINTS;
            break;
        default:
            newVal = "Solid";
            pm = Ogre::PM_SOLID;
            break;
        }

        mCamera->setPolygonMode(pm);
    } else if (arg.key == OIS::KC_SYSRQ)   // take a screenshot
    {
        mWindow->writeContentsToTimestampedFile("screenshot", ".jpg");
    }
    else if (arg.key == OIS::KC_ESCAPE)
    {
        mShutDown = true;
    }

    mCameraMan->injectKeyDown(arg);
    return true;
}

bool BaseApplication::keyReleased( const OIS::KeyEvent &arg )
{
    mCameraMan->injectKeyUp(arg);
    if (arg.key == OIS::KC_X) {
    }
    return true;
}

bool BaseApplication::mouseMoved( const OIS::MouseEvent &arg )
{
mTrayMgr->injectMouseMove(arg);
Ogre::Real screenWidth = Ogre::Root::getSingleton().getAutoCreatedWindow()->getWidth();
Ogre::Real screenHeight = Ogre::Root::getSingleton().getAutoCreatedWindow()->getHeight();

// convert to 0-1 offset
Ogre::Real offsetX = arg.state.X.abs / screenWidth;
Ogre::Real offsetY = arg.state.Y.abs / screenHeight;

// set up the ray
Ogre::Ray mouseRay = mCamera->getCameraToViewportRay(offsetX, offsetY);
Ogre::RaySceneQuery* mRayScnQuery = mSceneMgr->createRayQuery(mouseRay);
mRayScnQuery->setQueryMask(GROUND_OBJ);
mRayScnQuery->setSortByDistance(true);
Ogre::RaySceneQueryResult &result = mRayScnQuery->execute();
Ogre::RaySceneQueryResult::iterator itr;
for ( itr = result.begin( ); itr != result.end(); itr++ )  {
 if ( itr->movable ) {
	Ogre::Vector3 hitPoint;
	if(st->RaycastFromPoint(mouseRay.getOrigin(),mouseRay.getDirection(),hitPoint)) {
		st->drawMouse(hitPoint.x,hitPoint.z);
		}
 	  }
   }
}

bool BaseApplication::mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
    if (mTrayMgr->injectMouseDown(arg, id)) return true;
    mCameraMan->injectMouseDown(arg, id);
    return true;
}

bool BaseApplication::mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id){
   switch(id) {
        case OIS::MB_Left:
        	   Ogre::Real screenWidth  = Ogre::Root::getSingleton().getAutoCreatedWindow()->getWidth();
        	   Ogre::Real screenHeight = Ogre::Root::getSingleton().getAutoCreatedWindow()->getHeight();
        	   // convert to 0-1 offset
        	   Ogre::Real offsetX = arg.state.X.abs / screenWidth;
        	   Ogre::Real offsetY = arg.state.Y.abs / screenHeight;

        	   // set up the ray
        	   Ogre::Ray mouseRay = mCamera->getCameraToViewportRay(offsetX, offsetY);
        	   Ogre::RaySceneQuery* mRayScnQuery = mSceneMgr->createRayQuery(mouseRay);
        	   //
        	   mRayScnQuery->setSortByDistance(true);
        	   mRayScnQuery->setQueryMask(GROUND_OBJ);
        	   Ogre::RaySceneQueryResult &result = mRayScnQuery->execute();
        	   Ogre::RaySceneQueryResult::iterator itr;
        	   for ( itr = result.begin( ); itr != result.end(); itr++ )  {
        	    if ( itr->movable ) {
        	    	// Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::LML_NORMAL,itr->movable->getName());
        	    	// Ogre::Entity* cc = mSceneMgr->getEntity(itr->movable->getName());
        	    	// cc->getParentSceneNode()->showBoundingBox(true);

        	    	Ogre::Vector3 hitPoint;
						if (st->RaycastFromPoint(mouseRay.getOrigin(),mouseRay.getDirection(),hitPoint)) {
							if(player->move==true)
								 player->krok2=player->krok;
								else
								 player->krok2=player->cel;
							player->point=0;

							player->cel=Ogre::Vector3((hitPoint.x),(0),(hitPoint.z));
							player->kierunek=(player->cel-player->krok2).normalisedCopy();

							niniaNode1->lookAt(player->cel,Ogre::Node::TS_WORLD,Ogre::Vector3::NEGATIVE_UNIT_Z);
							niniaEnt1->getAnimationState("Walk")->setEnabled(true);
							player->move=true;
							return true;
						}
        	      }
        	   }
        break;
    }


    if (mTrayMgr->injectMouseUp(arg, id)) return true;
    mCameraMan->injectMouseUp(arg, id);
    return true;
}

//Adjust mouse clipping area
void BaseApplication::windowResized(Ogre::RenderWindow* rw)
{
    unsigned int width, height, depth;
    int left, top;
    rw->getMetrics(width, height, depth, left, top);

    const OIS::MouseState &ms = mMouse->getMouseState();
    ms.width = width;
    ms.height = height;
}

//Unattach OIS before window shutdown (very important under Linux)
void BaseApplication::windowClosed(Ogre::RenderWindow* rw){
    //Only close for window that created OIS (the main window in these demos)
    if(rw == mWindow){
     if(mInputManager){
        mInputManager->destroyInputObject( mMouse );
        mInputManager->destroyInputObject( mKeyboard );

        OIS::InputManager::destroyInputSystem(mInputManager);
        mInputManager = 0;
        }
    }
}

